import React from 'react';
import PropTypes from 'prop-types';

const AUINavGroup = props => {
    return (
        <nav className="aui-navgroup aui-navgroup-vertical">
            <div className="aui-navgroup-inner">
                {props.children}
            </div>
        </nav>
    );
};

AUINavGroup.propTypes = {
    children: PropTypes.node
};

export default AUINavGroup;
