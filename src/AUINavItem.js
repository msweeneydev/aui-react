import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const AUINavItem = ({ selected, href, children }) => {
    const classes = classnames({'aui-nav-selected': selected});

    return (
        <li className={classes}>
            <a href={href} className="aui-nav-item">{children}</a>
        </li>
    );
};

AUINavItem.propTypes = {
    href: PropTypes.string,
    selected: PropTypes.bool,
    children: PropTypes.oneOfType([
        PropTypes.node,
        PropTypes.string
    ])
};

export default AUINavItem;
