import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const AUIMessage = ({type, title, children, isCloseable, onClose}) => {
    let typeClass;
    if (type) {
        typeClass = `aui-message-${type}`;
    }

    const classes = classnames('aui-message', typeClass, {
        closeable: isCloseable
    });

    return (
        <div className={classes}>
            {!title ? null : <p className="title"><strong>{title}</strong></p>}
            { isCloseable ? <span className="aui-icon icon-close" role="button" tabIndex="0" onClick={onClose}></span> : null }
            {children}
        </div>
    );
};

AUIMessage.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.node
    ]).isRequired,
    title: PropTypes.string,
    isCloseable: PropTypes.bool,
    type: PropTypes.oneOf(['warning', 'error', 'success', 'hint', 'info']),
    /**
     * Callback for closing AUIMessage
     */
    onClose: PropTypes.func,
};

export default AUIMessage;
