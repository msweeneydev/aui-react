import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const AUIAvatar = ({ isProject, size, src }) => {
    const classes = classnames('aui-avatar', `aui-avatar-${size}`, {
        'aui-avatar-project': isProject === true
    });
    return (
        <span className={classes}>
            <span className="aui-avatar-inner">
                <img src={src}/>
            </span>
        </span>
    );
};

AUIAvatar.propTypes = {
    src: PropTypes.string,
    size: PropTypes.oneOf(['xsmall', 'small', 'medium', 'large', 'xlarge', 'xxlarge', 'xxxlarge']),
    isProject: PropTypes.bool
};

export default AUIAvatar;
