import React from 'react';
import { mount } from 'enzyme';
import { spy } from 'sinon';
import { expect } from 'chai';

import Tab from '../src/Tabs/Tab';
import Tabs from '../src/Tabs/Tabs';

describe('Tabs', () => {
    const defaultMockConfig = {
        lastIsActive: false,
        tabsId: null,
        tabsClassName: null,
        tabsProps: {}
    };

    const mountComponent = (mockConfig = defaultMockConfig) => (
        mount(
            <Tabs
                className={mockConfig.tabsClassName}
                id={mockConfig.tabsId}
                {...mockConfig.tabsProps}
            >
                <Tab label="First Tab" id="first-tab">
                    Content of first tab
                </Tab>
                <Tab label="Seconds Tab" id="second-tab">2</Tab>
                <Tab label="Third Tab" id="third-tab" isActive={mockConfig.lastIsActive}>
                    <p>Content of 3rd Tab</p>
                </Tab>
            </Tabs>
        )
    );

    describe('Structure', () => {
        it('should render Tabs', () => {
            const component = mountComponent();

            const tabs = component.find('.menu-item');
            const tabsContent = component.find('.tabs-pane');

            expect(tabs).to.have.length(3);
            expect(tabsContent).to.have.length(3);
        });

        it('should render first tab as active one by default', () => {
            const component = mountComponent();

            const activeTab = component.find('.active-tab');
            const activePane = component.find('.active-pane');

            expect(activeTab).to.have.text('First Tab');
            expect(activePane).to.have.text('Content of first tab');
        });

        it('should render last tab as active one', () => {
            const component = mountComponent({
                lastIsActive: true
            });

            const activeTab = component.find('.active-tab');
            const activePane = component.find('.active-pane');

            expect(activeTab).to.have.text('Third Tab');
            expect(activePane).to.have.text('Content of 3rd Tab');
        });

        it('should allow to pass custom class name to <Tabs> wrapper', () => {
            const component = mountComponent({
                tabsClassName: 'foo'
            });

            const tabs = component.find('.aui-tabs');

            expect(tabs).to.have.className('foo');
        });


        it('should allow to pass custom id to <Tabs> wrapper', () => {
            const component = mountComponent({
                tabsId: 'bar'
            });

            const tabs = component.find('.aui-tabs');

            expect(tabs).to.have.attr('id', 'bar');
        });

        it('should allow to pass custom props to <Tabs> wrapper', () => {
            const component = mountComponent({
                tabsProps: {
                    'data-attr': 'moo'
                }
            });

            const tabs = component.find('.aui-tabs');

            expect(tabs).to.have.attr('data-attr', 'moo');
        });
    });

    describe('Interaction integration', () => {
        it('should switch active tab from 1st to 3rd one', () => {
            // given
            const component = mountComponent();
            const thirdTab = component.find('.menu-item a').at(2);

            // when
            thirdTab.simulate('click');

            // then
            const activeTab = component.find('.active-tab');
            const activePane = component.find('.active-pane');
            expect(activeTab).to.have.text('Third Tab');
            expect(activePane).to.have.text('Content of 3rd Tab');
        });

        it('should switch from last to 1st tab', () => {
            // given
            const component = mountComponent({
                lastIsActive: true
            });
            const firstTab = component.find('.menu-item a').at(0);

            // when
            firstTab.simulate('click');

            // then
            const activeTab = component.find('.active-tab');
            const activePane = component.find('.active-pane');
            expect(activeTab).to.have.text('First Tab');
            expect(activePane).to.have.text('Content of first tab');
        });
    });
});

describe('Tab', () => {
    const defaultMockConfig = {
        className: null
    };

    const mountComponent = (mockConfig = defaultMockConfig) => (
        mount(
            <Tabs>
                <Tab label="First Tab" id="first-tab" className={mockConfig.className}>
                    Content of first tab
                </Tab>
            </Tabs>
        )
    );

    it('should allow to pass custom class names to <Tab> component', () => {
        const component = mountComponent({
            className: 'foo bar'
        });

        const tab = component.find('.tabs-pane').first();

        expect(tab).to.have.className('foo');
        expect(tab).to.have.className('bar');
    });

});
