import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import AUINavItem from '../src/AUINavItem';

describe('AUINavItem', () => {
    it('should render the correct AUI markup', () => {
        expect(shallow(<AUINavItem />).html()).to.equal(`<li class=""><a class="aui-nav-item"></a></li>`);
    });

    it('should render the correct AUI markup for a selected nav item', () => {
        expect(shallow(<AUINavItem selected />).html()).to.equal(`<li class="aui-nav-selected"><a class="aui-nav-item"></a></li>`);
    });
});
